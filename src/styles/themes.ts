const theme = {
  colors: {
    primary: '#0277bd',
    secondary: '#ff8f00',

    primaryLight: '#58a5f0',
    secondaryLight: '#ffc046',

    primaryDark: '#004c8c',
    secondaryDark: '#c56000',

    textNormal: '#707070',

    textOnPrimary: '#ffff',
    textOnSecondary: '#010101',

    background: '#fff',
  },
  breakpoints: {
    mobile: 768,
    tabletMin: 769,
    tabletMax: 1024,
    desktopMin: 1025,
  },
};

export default theme;
